package de.andreasfritz.converter.model;

import java.util.Date;
import lombok.Data;

@Data
public class Attachment {

  private String id;
  private String series_id;
  private String file_service_id;
  private String external_id;
  private String source;
  private String name;
  private String link;
  private String extra_data;
  private String size;
  private Date date_created;
  private Date date_modified;
}
