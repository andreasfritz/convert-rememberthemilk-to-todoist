package de.andreasfritz.converter.model;

import java.util.Date;
import lombok.Data;

@Data
public class Task {

  private String id;
  private String series_id;
  private String list_id;
  private String name;
  private String priority;
  private Date date_created;
  private Date date_added;
  private Date date_modified;
  private Date date_completed;
  private Date date_due;
  private boolean date_due_has_time;
  private boolean date_start_has_time;
  private int postponed;
  private String source;
  private boolean repeat_every;
  private String[] tags;
  private String location_id;
  private String url;
  private String repeat;
  private String parent_id;
}
