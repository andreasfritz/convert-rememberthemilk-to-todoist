package de.andreasfritz.converter.model;

import java.util.Date;
import lombok.Data;

@Data
public class TaskList {

  private String id;
  private String name;
  private Date date_created;
  private Date date_modified;
  private Date date_archived;
  private boolean syncable;
  private String sorting_scheme_id;
  private String relative_position;
}
