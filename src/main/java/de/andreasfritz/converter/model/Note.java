package de.andreasfritz.converter.model;

import java.util.Date;
import lombok.Data;

@Data
public class Note {

  private String id;
  private String series_id;
  private Date date_created;
  private Date date_modified;
  private String title;
  private String content;
  private String creator_id;
  private String last_editor_id;
}
