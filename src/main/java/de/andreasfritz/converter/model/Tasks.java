package de.andreasfritz.converter.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tasks {

  private List<Task> tasks;
  private List<Note> notes;
  private List<Attachment> attachments;
  private List<TaskList> lists;
}
