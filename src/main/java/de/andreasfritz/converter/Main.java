package de.andreasfritz.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.andreasfritz.converter.model.Attachment;
import de.andreasfritz.converter.model.Note;
import de.andreasfritz.converter.model.Task;
import de.andreasfritz.converter.model.TaskList;
import de.andreasfritz.converter.model.Tasks;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

@Slf4j
public class Main {

  public static final String DATE_LANG = "de";
  public static final String DATE_FORMAT = "dd.MM.yyyy";

  public static void main(String[] args) throws IOException {

    Tasks tasks = readRTM();

    filterCompletedTasks(tasks);

    logTasks(tasks);

    writeTodoist(tasks);
  }

  private static Tasks readRTM() throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.readValue(new File("rememberthemilk_export.json"), Tasks.class);
  }

  private static void filterCompletedTasks(Tasks tasks) {
    List<Task> taskList =
        tasks.getTasks().stream()
            .filter(t -> t.getDate_completed() == null)
            .collect(Collectors.toList());
    tasks.setTasks(taskList);
  }

  private static void logTasks(Tasks tasks) throws JsonProcessingException {
    ObjectMapper objectMapper =
        new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);
    String export = objectMapper.writeValueAsString(tasks);

    log.info(export);
  }

  private static void writeTodoist(Tasks tasks) throws IOException {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(
        "TYPE,CONTENT,DESCRIPTION,PRIORITY,INDENT,AUTHOR,RESPONSIBLE,DATE,DATE_LANG,TIMEZONE");
    stringBuilder.append(System.lineSeparator());

    List<Task> taskList = tasks.getTasks();
    for (Task task : taskList) {
      stringBuilder.append("task,");
      stringBuilder.append('"').append(task.getName());
      String[] tags = task.getTags();
      for (String tag : tags) {
        stringBuilder.append(" @").append(tag);
      }
      stringBuilder.append('"').append(",");
      Optional<TaskList> list =
          tasks.getLists().stream().filter(l -> l.getId().equals(task.getList_id())).findFirst();
      if (list.isPresent()) {
        stringBuilder
            .append('"')
            .append("List: ")
            .append(list.get().getName())
            .append('"')
            .append(",");
      } else {
        stringBuilder.append(",");
      }
      stringBuilder.append(StringUtils.remove(task.getPriority(), "P")).append(",,,,");
      if (task.getDate_due() != null) {
        stringBuilder
            .append(simpleDateFormat.format(task.getDate_due()))
            .append("," + DATE_LANG + ",");
      } else {
        stringBuilder.append(",,");
      }
      stringBuilder.append(System.lineSeparator());
      if (task.isRepeat_every()) {
        stringBuilder.append("note,");
        stringBuilder.append('"').append("Repeat: ").append(task.getRepeat()).append('"');
        stringBuilder.append(",,,,,,,");
        stringBuilder.append(System.lineSeparator());
      }
      List<Note> notes =
          tasks.getNotes().stream()
              .filter(n -> n.getSeries_id().equals(task.getSeries_id()))
              .collect(Collectors.toList());
      for (Note note : notes) {
        stringBuilder.append("note,");
        stringBuilder
            .append('"')
            .append(StringUtils.replace(note.getContent(), System.lineSeparator(), " / "))
            .append('"');
        stringBuilder.append(",,,,,,,");
        stringBuilder.append(System.lineSeparator());
      }
      List<Attachment> attachments =
          tasks.getAttachments().stream()
              .filter(a -> a.getSeries_id().equals(task.getSeries_id()))
              .collect(Collectors.toList());
      for (Attachment attachment : attachments) {
        stringBuilder.append("note,");
        stringBuilder
            .append('"')
            .append(attachment.getName())
            .append(" - ")
            .append(attachment.getLink())
            .append('"');
        stringBuilder.append(",,,,,,,");
        stringBuilder.append(System.lineSeparator());
      }
    }

    File file = new File("todoist_import.csv");
    org.apache.commons.io.FileUtils.writeStringToFile(
        file, stringBuilder.toString(), Charset.defaultCharset());
  }
}
